var bob = {
	_name: "Bob",
	_friends: [],
	printFriends() {
		this._friends.forEach(f =>
			console.log(this._name + " knows " + f));
	}
};

bob._friends = ["alex", "Chuchin"];
bob.printFriends();