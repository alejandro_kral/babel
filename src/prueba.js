var bob = {
	_name: "Bob",
	_friends: [],
	printFriends() {
		this._friends.forEach(f =>
			console.log(this._name + " knows " + f));
	}
};

console.log("hi");

let imprime_mensaje = ( nombre = 'Alex' ) => console.log( `Hola ${nombre}` );

bob._friends = ["alex", "Chuchin"];
bob.printFriends();
imprime_mensaje();
imprime_mensaje("Ernesto");