"use strict";

var bob = {
	_name: "Bob",
	_friends: [],
	printFriends: function printFriends() {
		var _this = this;

		this._friends.forEach(function (f) {
			return console.log(_this._name + " knows " + f);
		});
	}
};

console.log("hi");

var imprime_mensaje = function imprime_mensaje() {
	var nombre = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'Alex';
	return console.log("Hola " + nombre);
};

bob._friends = ["alex", "Chuchin"];
bob.printFriends();
imprime_mensaje();
imprime_mensaje("Ernesto");