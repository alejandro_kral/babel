"use strict";

var bob = {
	_name: "Bob",
	_friends: [],
	printFriends: function printFriends() {
		var _this = this;

		this._friends.forEach(function (f) {
			return console.log(_this._name + " knows " + f);
		});
	}
};

bob._friends = ["alex", "Chuchin"];
bob.printFriends();