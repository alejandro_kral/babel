var path = require('path');
var webpack = require('webpack');

module.exports = {
	entry: path.join( __dirname, '/src'),
	output: {
		path: "./dist",
		filename: "[name].js"
	},
	module: {
		loaders: [{
			test: /\.js$/,
			loaders: [ 'babel' ],
			exclude: /node_modules/
		}]
	}/*,
	plugins: [
		new webpack.optimize.UglifyJsPlugin({
			compress: {
				warnings: false
			}
		})
	]*/
};